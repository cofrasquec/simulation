#ifndef TP2_1_H
#define TP2_1_H
	
	// TP2_1.h
	// TP 2 : Lab # 2– Learning and Assessment Situations
	//                  Generation of Random Variates

// pour avoir l'explication des fonctions, il faut regarder le .c

double uniform(double a, double b);

void display_three_populations(int n);

double *generic_population(int n, double observations[]);

double *cumulative_generic_population(int n, double observations[]);

void display_generic_population(int n, double observations[], int sample_size);

double neg_exp(double mean);

double *frequency_test(int sample_size);

void fourty_dices_throw(int sample_size);

int intervalle_of_x(double x);

void box_and_muller(int sample_size);


#endif