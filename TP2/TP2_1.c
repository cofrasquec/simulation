#include <stdio.h>
#include <stdlib.h>
#include <math.h> // pour roundf() et log()

#include "mt19937ar/mt19937ar.h"
#include "TP2_1.h"

	// TP2_1.c
	// TP 2 : Lab # 2– Learning and Assessment Situations
	//                  Generation of Random Variates


// 2 - Generation of uniform random numbers between A and B

/* uniform(): Draw a random number belonging to the [a - b] interval, with a uniform probability.
 *
 * Parameters: a, b -> two doubles such as a < b; boundary markers for the interval
 *
 * Output: double; the random number drawn.
 */

double uniform(double a, double b){
	return a + (b - a) * genrand_real1();
}


// 3 - Reproduction of discrete empirical distributions

// 3.A
/* display_three_populations(): Simulate the discrete distribution of classes A, B, C following the wording.
 *
 * Parameters: sample_size -> int ; the number of drawing for the simulation.
 *
 * Output: no output, display the results
 */
void display_three_populations(int sample_size){
	double a = 0.0;
	double b = 0.0;
	double c = 0.0;

	for (int i = 0; i < sample_size; i++){
		double choix = genrand_real1();
		if (choix <= 0.35)
			a++;
		else if (choix <= 0.8) // 0,35 + 0,45
			b++;
		else
			c++;
	}

	printf("Sur %d tirages, on obtient la répartition suivante :\n\tA : %f\n\tB : %f\n\tC : %f\n", sample_size, a/sample_size, b/sample_size, c/sample_size);
}


// 3.B.a
/* generic_population(): Calculate the probability of being in each class starting from a given observation
 *
 * Parameters: n -> int; the number of classes of the observation
 *				observation -> array of double; the observation of a population.
 *
 * Output: double pointer; display the probability of belonging to each class
 */
double *generic_population(int n, double observations[]){
	double maximum = 0.0;

	for (int i = 0; i < n; i++)
		maximum += observations[i];

 	double *repartition = (double *)malloc(n * sizeof(double));

 	for (int i = 0; i < n; i++)
 		repartition[i] = observations[i]/maximum;

 	return repartition;
}


// 3.B.b
/* cumulative_generic_population(): Give the cumulative probabilities of being in each class starting from a given observation
 *
 * Parameters  n -> int; the number of classes of the observation
 *				observation -> array of double; the observation of a population.
 *
 * Output: double pointer; display the cumulative probabilities
 */
double *cumulative_generic_population(int n, double observations[]){
	double maximum = 0.0;

	for (int i = 0; i < n; i++)
		maximum += observations[i];

 	double *cumulative = (double *)malloc(n * sizeof(double));
 	double sum = 0.0;

 	for (int i = 0; i < n; i++){
 		sum += observations[i]/maximum;
 		cumulative[i] = sum;
 	}

 	return cumulative;
}


// 3.B.c
/* display_generic_population(): Repeat the cumulative_generic_population() function sample_size times
 *
 * Parameters: n -> int; the number of classes of the observation
 *				observation -> array of double; the observation of a population.
 *				sample_size -> int; the number of drawings
 *
 * Output: no output; display the cumulated probabilities
 */
void display_generic_population(int n, double observations[], int sample_size){
	int *simulated_distribution = (int *)malloc(n * sizeof(int));
	double *cumulative = cumulative_generic_population(n, observations);

	for(int i = 0; i<n; i++)
		simulated_distribution[i] = 0;

	for (int i = 0; i < sample_size; i++){
		double choix = genrand_real1();
		int j = 0;
		while (j < n && choix > cumulative[j])
			j++;
		simulated_distribution[j]++;
	}

	printf("Tableau de distribution simulées pour %d tirages à partir des données fournit en entrées:\n", sample_size);
	for (int i = 0; i < n; i++)
		printf("%d ", simulated_distribution[i]);
	printf("\n");

	free(simulated_distribution);
	free(cumulative);
}


// 4 - Reproduction of continuous distributions

// 4.A
/* neg_exp(): Calculate the negative exponential abscissa with the given mean from a random ordinate
 *
 * Parameters: mean -> int; needed for the formula
 *
 * Output: double; the abscissa
 */
double neg_exp(double mean){
	return -1 * mean * log(1-genrand_real1());
}


// 4.C
/* frequency_test(): Discrete distribution of a biased dice (i.e. using the neg_exp())
 *
 * Parameters: sample_size -> int; the number of drawings
 *
 * Output: double pointer ; indicate the frequency of numbers between 0 and 1, between 1 and 2, ..., above 20
 */
double *frequency_test(int sample_size){
	double *frequency = (double *)malloc(21 * sizeof(double));

	for (int i = 0; i < sample_size; i++){
		double tmp = neg_exp(10);
		if (tmp < 20.0)
			frequency[(int)tmp]++;
		else
			frequency[20]++;
	}

	for (int i = 0; i < 21; i++)
		frequency[i] /= sample_size;

	return frequency;
}


// 5 - Simulating non reversible distribution laws

// 5.1 First implementation

/* fourty_dice_throws(): Add fourty dice throws up several times to get the mean and the standard deviation of it
 * 
 * Parameters: sample_size -> int; the number of drawings
 *
 * Output : no output; display directly the results
 */
void fourty_dice_throws(int sample_size){
	long int *score = (long int *)malloc(201 * sizeof(long int)); // il y a 201 valeurs entre 40 et 240 inclus
	double sum_sum = 0.0; // for practical mean
	double extent_from_mean = 0.0; // for standard deviation

	for (int i = 0; i < sample_size; i++){
		int sum = 0;
		for (int j = 0; j < 40; j++)
			sum += (int)uniform(1.0, 7.0);
		score[sum-40]++;
		sum_sum += sum;
		extent_from_mean += (sum - 140) * (sum - 140); // 140 = theorical mean
	}

	printf("\n40 dices throws repeated %d fois, here is the repartition :\n", sample_size);
	for (int i = 0; i < 201; i++){
		for (int j = 0; j < (score[i] / (sample_size / 1000)); j++)
			printf("X");
		printf("\t<- Case %d\n", i+40);
	}

	printf("The mean of the drawings is: %f\n", sum_sum / sample_size);
	printf("The deviation is (using the theoring median (= 140)): %f\n", sqrt(extent_from_mean / sample_size));
	free(score);
}


// 5.2 Test of an analytical model of the Gaussian distribution

/* intervalle_of_x(): give the interval of x to ge ta gaussian representation. it is used in the box_and_muller() function
 * 
 * Parameters: sample_size -> int; the number of drawings
 *
 * Output : no output; display directly the results
 */
int intervalle_of_x(double x){
	if (x <= -5.0)
		return 0;
	else if (x >= 5.0)
		return 99;
	else{
		return (int)(x * 10 + 50);
	}
}


/* box_and_muller(): Get a representation of the gaussian function thanks to the Box and Muller formula and a lot of drawings
 * 
 * Parameters: sample_size -> int; the number of drawings, i.e. the resolution of the gaussian modelisation
 *
 * Output : no output; display directly the results
 */
void box_and_muller(int sample_size){
	int *analytical_gaussian = (int *)malloc(100 * sizeof(int));
	double sum_sum = 0.0;
	double extent_from_mean = 0.0;

	for (int i = 0; i < sample_size; i++){
		double R_n1 = genrand_real1();
		double R_n2 = genrand_real1();
		double x_1 = cos(2 * M_PI * R_n2) * sqrt(-2 * log(R_n1)); // log() = ln et log10() = log
		double x_2 = sin(2 * M_PI * R_n2) * sqrt(-2 * log(R_n1));

		sum_sum += x_1 + x_2;
		extent_from_mean += x_1 * x_1 + x_2 * x_2; // because the theoretical mean = 0;

		analytical_gaussian[intervalle_of_x(x_1)]++;
		analytical_gaussian[intervalle_of_x(x_2)]++;
	}

	printf("\nBox and Muller algorithm with %d iterations :\n", sample_size);
	for (int i = 0; i < 100; i++){
		for (int j = 0; j < (analytical_gaussian[i] / (sample_size / 1000)); j++)
			printf("X");
		printf("\t<- Case %d\n", i);
	}
	printf("The mean of the drawings is: %f\n", sum_sum / (2 * sample_size));
	printf("The deviation is (using the theoring median (= 0)): %f\n", sqrt(extent_from_mean / (2 * sample_size)));
}


int main(){
	//gcc -Wall -Wextra -Werror -g -O2 -o bonjour1.out mt19937ar/mt19937ar.c TP2_1.c -lm
	unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);


	printf("Tests de la partie 2 :\n");
	printf("Température entre -98°C et 57,7°C : %f\n", roundf(uniform(-98, 57.7) *10) /10); // ca arrondit mais ca laissee les zeros
	printf("Température entre -98°C et 57,7°C : %10.1f\n", uniform(-98, 57.7));


	printf("\nTests de la partie 3 :\n");
	display_three_populations(1000);
	display_three_populations(10000);
	display_three_populations(100000);
	display_three_populations(1000000);
	int taille = 6;
	double obs[] = {100, 400, 600, 400, 100, 200};
	double *repar = generic_population(taille, obs);
	printf("Tableau de repartition des données :\n");
	for (int i=0; i<taille; i++)
		printf("%f ", repar[i]);
	printf("\n");
	double *cumul = cumulative_generic_population(taille, obs);
	printf("Tableau de repartition cumulative des données :\n");
	for (int i=0; i<taille; i++)
		printf("%f ", cumul[i]);
	printf("\n");
	display_generic_population(taille, obs, 1000);
	display_generic_population(taille, obs, 1000000);


	printf("\nTests de la partie 4 :\n");
	printf("Moyenne = 10, negExp = %f\n", neg_exp(10.0));
	double average = 0.0;
	for (int i=0; i<1000; i++)
		average += neg_exp(10.0);
	printf("1 000 tirages, moyenne = 10, negExp(moyen) = %f\n", average/1000);
	average = 0.0;
	for (int i=0; i<1000000; i++)
		average += neg_exp(10.0);
	printf("1 000 000 tirages, moyenne = 10, negExp(moyen) = %f\n", average/1000000);
	double *freq = frequency_test(1000);
	printf("Test des frequences de tirages de neg_exp pour 1 000 tirages :\n(Entre 0 et 1, entre 1 et 2, ..., entre 19 et 20, supérieur à 20)\n");
	for (int i=0; i<21; i++)
		printf("%f ", freq[i]);
	freq = frequency_test(1000000);
	printf("\nTest des frequences de tirages de neg_exp pour 1 000 000 tirages :\n(Entre 0 et 1, entre 1 et 2, ..., entre 19 et 20, supérieur à 20)\n");
	for (int i=0; i<21; i++)
		printf("%f ", freq[i]);
	printf("\n");

	
	printf("\nTests de la partie 5 :\n");
	fourty_dice_throws(1000);
	fourty_dice_throws(1000000);
	box_and_muller(1000);
	box_and_muller(1000000); // je teste plusieurs pcq il y a des fois où ça va bien et d'autres où des anomalies apparaissent
	box_and_muller(1000000); // et je comprends pas pourquoi....
	box_and_muller(1000000); // je me dis que c'est peut être un effet de bord
	box_and_muller(1000000);
	box_and_muller(1000000);
	box_and_muller(1000000);
	box_and_muller(1000000000);
	

	free(repar);
	free(cumul);
	free(freq);
	return 0;
}