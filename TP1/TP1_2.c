#include <stdio.h>
#include <stdlib.h>

    // TP 1 : Lab. #1 – Learning and Assessment Situations
    //         Modeling & Simulating Randomness with
    //         deterministic “pseudo-random” number generators


int position_intRand = 0; // pour la suite intRand()

// C – Crafting linear congruential generators

// Question 7
long int premier_generateur(long int graine){
    return (5 * graine + 1) % 16;
}

int intRand(){
    position_intRand = position_intRand % 32; // en vrai on pourrait juste mettre %16 car elle a un cycle de 16
    int i = position_intRand;
    position_intRand ++;
    int res = 10;
    for (int j = 0; j < i; j++)
        res = premier_generateur(res);
    return res;
}

float floatRand(){
    return intRand() / 16.0;
}


// Question 8
long int deuxieme_generateur(long int graine){
    return (666 * graine + 666) % 6666;
}

long int troisieme_generateur(long int graine){
    return (251 * graine + 32102) % 2254254;
}

long int puissance(int base, int exposant){
    if (exposant == 0)
        return 1;
    long int res = base;
    while (exposant > 1){
        res *= base;
        exposant --;
    }
    return res;
}

long int quatrieme_generateur(long int graine){ // ou uint64_t
    return (22695477 * graine + 1) % puissance(2, 31);
}


// Question 9
// https://en.wikipedia.org/wiki/Linear_congruential_generator
// formule : X_n+1 = (a * X_n + c) mod m
// m prime, c = 0 : Often a prime just less than a power of 2 is used (the Mersenne primes 2^31−1 and 2^61−1 are popular)
// m a power of 2, c = 0 : most often m = 2^32 or m = 2^64
// m a power of 2, c != 0 : m and c are coprime, a−1 is divisible by all prime factors of m, a−1 is divisible by 4 if m is divisible by 4.
// few examples : m | a | c
// 2^16+1 | 75 | 74 
// 2^32 | 1664525 | 1013904223
// 2^31 | 22695477 | 1


// D - Miscellaneous

// Question 10
//


int main(){
    //gcc -Wall -Werror -Wextra -o bonjour2 TP1_2.c
    // Test Question 7
    // 1ere version
    long int graine = 10;
    printf("Les 32 premiers termes, pour le premier générateur : (1ere façon)\n");
    for (int i = 0; i < 32; i++){
        printf("%ld ", graine);
        graine = premier_generateur(graine);
    }
    printf("\n");
    // 2eme version
    printf("Les 32 premiers termes, pour le premier générateur : (2eme façon)\n");
    for (int i = 0; i < 32; i++)
        printf("%d ", intRand());
    printf("\n\n");
    //floatRand()
    printf("Les 32 premiers termes, pour le premier générateur : (2eme façon, version flottante)\n");
    for (int i = 0; i < 32; i++)
        printf("%f ", floatRand());
    printf("\n\n");

    // Test Question 8
    graine = 6;
    printf("Les 32 premiers termes, pour le deuxieme générateur :\n");
    for (int i = 0; i < 32; i++){
        printf("%ld ", graine);
        graine = deuxieme_generateur(graine);
    }
    printf("\n");
    graine = 15316;
    printf("Les 32 premiers termes, pour le troisieme générateur :\n");
    for (int i = 0; i < 32; i++){
        printf("%ld ", graine);
        graine = troisieme_generateur(graine);
    }
    printf("\n");
    printf("2 puissance 6 = %ld\n", puissance(2, 6));
    graine = 56;
    printf("Les 32 premiers termes, pour le quatrieme générateur :\n");
    for (int i = 0; i < 32; i++){
        printf("%ld ", graine);
        graine = quatrieme_generateur(graine);
    }
    printf("\n");


    return 0;
}