#include <stdio.h>
#include <stdlib.h>

    // TP 1 : Lab. #1 – Learning and Assessment Situations 
    //         Modeling & Simulating Randomness with
    //         deterministic “pseudo-random” number generators


// A – Nonlinear generator :

// Question 1
int middle_square(int depart){
    int res = depart * depart;
    res /= 100; // 1ere etape : j'enleve le debut
    res = res % 10000; // 2eme etape : j'enleve le debut
    return res;
}


// Question 2
void test_middle_square_and_display(int depart){
    printf("\nAu depart on a : %d\n", depart);
    for(int i=1; i <=30; i++){
        depart = middle_square(depart);
        printf("A l'etape %d : on a %d\n", i, depart);
    }
}


// Question 3
/* Dans le manuel : The function rand() is not reentrant, since it uses hidden state  that  is  modified  on  each call.
    Donc elle n'est pas utilisable dans un programme que l'on cherche à debugger.
*/


// B – Basic use of randomness: coin tossing, dices…

// Question 4 : la piece
void toss(int nb_runs){ 
    int nb_faces = 0;
    for (int i = 0; i < nb_runs; i++){
        if (rand() %2)
            nb_faces++;
    }
    printf("J'ai eu %d faces sur %d lancers.\n", nb_faces, nb_runs);
}


// Question 5 : le dé à 6 faces
void lancer_de_de_six(int nb_runs){ 
    int resultat_de[] = {0, 0, 0, 0, 0, 0}; // attention le nombre de 6 est dans la premiere case
    for (int i = 0; i < nb_runs; i++)
        resultat_de[rand() %6] ++;
    printf("J'ai eu %d uns, %d deux, %d trois, %d quatres, %d cinqs, %d six; sur %d lancers.\n", resultat_de[1], resultat_de[2], resultat_de[3], resultat_de[4], resultat_de[5], resultat_de[0], nb_runs);
}


// Question 6 : le dé à 10 faces
void lancer_de_de_dix(int nb_runs){ 
    int resultat_de[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // attention le nombre de 6 est dans la premiere case
    for (int i = 0; i < nb_runs; i++)
        resultat_de[rand() %10] ++;
    printf("J'ai eu %d uns, %d deux, %d trois, %d quatres, %d cinqs, %d six, %d sept, %d huits, %d neufs, %d dix; sur %d lancers.\n", resultat_de[1], resultat_de[2], resultat_de[3], resultat_de[4], resultat_de[5], resultat_de[6], resultat_de[7], resultat_de[8], resultat_de[9], resultat_de[0], nb_runs);
}


// C – Crafting linear congruential generators

int main(){
    //gcc -Wall -Werror -Wextra -o bonjour1 TP1_1.c
    // Tests Question 2
    int test1 = 1234;
    int test2 = 4100; // cycle de 4
    int test3 = 1324; // pseudo cycle de 4
    int test4 = 1301; // pseudo cycle
    int test5 = 1234; // Convergence vers 0
    int test6 = 3141; // Convergence vers 100 (étape 7)
    test_middle_square_and_display(test1);
    test_middle_square_and_display(test2);
    test_middle_square_and_display(test3);
    test_middle_square_and_display(test4);
    test_middle_square_and_display(test5);
    test_middle_square_and_display(test6);
    printf("\n");
    // Tests Question 4
    for (int i = 0; i < 5; i++){
        toss(10);
        toss(100);
        toss(1000);
    }
    printf("\n");
    // Tests Question 5
    for (int i = 0; i < 5; i++){
        lancer_de_de_six(10);
        lancer_de_de_six(100);
        lancer_de_de_six(1000);
    }
    printf("\n");
    // Tests Question 6
    for (int i = 0; i < 5; i++){
        lancer_de_de_dix(10);
        lancer_de_de_dix(100);
        lancer_de_de_dix(1000);
    }
    printf("\n");

    return 0;
}