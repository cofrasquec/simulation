package TP4;

import java.util.*;

public class Rabbit{
	private int maturity = 0; // 0 = child, 1 = adult

	private List<Rabbit> descendants = new ArrayList<>();
	private int nb_descendants = 0;

	public Rabbit(){}
	public Rabbit(int maturity){
		setMaturity(maturity);
	}

	public int getMaturity(){
		return maturity;
	}
	public void setMaturity(int maturity){
		this.maturity = maturity;
	}

	public List<Rabbit> getDescendants(){
		return descendants;
	}
	public void setDescendants(List<Rabbit> descendants){
		this.descendants = descendants;
	}

	public int getNbDescendants(){
		return nb_descendants;
	}
	public void setNbDescendants(int nb_descendants){
		this.nb_descendants = nb_descendants;
	}


	public void reproductionFibo(){
		if (getMaturity() == 0)
			setMaturity(1);
		else{
			for (int i = 0; i < getNbDescendants(); i++){
				getDescendants().get(i).reproductionFibo();
			}
			getDescendants().add(0, (new Rabbit()));
			setNbDescendants(getNbDescendants() + 1);
		}
	}

	public void displayRabbit(){
		if (getMaturity() == 1)
			System.out.print("00 ");
		else
			System.out.print("oo ");
	}

	public void displayFamily(){
		displayRabbit();
		for (int i = 0; i < getNbDescendants(); i++){
			getDescendants().get(i).displayFamily();
		}
	}


	public static void main(String[] argv){}
}