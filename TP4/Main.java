package TP4;

public class Main{

	public static void main(String[] argv){
		Rabbit forefather = new Rabbit();

		int iterations = 10;
		System.out.println("Pour dix itérations :");
		forefather.displayFamily();
		System.out.println("");
		for (int i = 0; i < iterations; i++){
			forefather.reproductionFibo();
			forefather.displayFamily();
			System.out.println("");
		}
	}
}