import src.*;


public class Main{
	public static void main(String[] argv){
		// javac -d class/ Main.java
		// java -classpath class/ Main
		// java -cp class/ Main

		Rabbit forefather = new Rabbit();

		int iterations = 15;
		System.out.printf("Pour dix itérations :\nItération 0\n");
		forefather.displayFamily();
		System.out.println("\n");
		for (int i = 0; i < iterations; i++){
			System.out.println("Itération " + (i+1));
			forefather.reproductionFibo();
			forefather.displayFamily();
			System.out.println("\n");
		}
	}
}