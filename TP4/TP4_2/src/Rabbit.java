package src;

import java.util.*;

public class Rabbit{
	/** Attributes */
	private static final SEXUAL_MATURITY = 7; // change according to species

	private int age = 0; // in months

	private List<Rabbit> descendants = new ArrayList<>();
	private int nb_descendants = 0;
	private int colour_number = 0;


	/** Builders */
	public Rabbit(){}
	public Rabbit(int maturity){
		setMaturity(maturity);
	}
	public Rabbit(int maturity, int colour_number){
		setMaturity(maturity);
		setColourNumber(colour_number);
	}


	/** Getters / Setters */
	public int getMaturity(){
		return maturity;
	}
	public void setMaturity(int maturity){
		this.maturity = maturity;
	}

	public List<Rabbit> getDescendants(){
		return descendants;
	}
	public void setDescendants(List<Rabbit> descendants){
		this.descendants = descendants;
	}

	public int getNbDescendants(){
		return nb_descendants;
	}
	public void setNbDescendants(int nb_descendants){
		this.nb_descendants = nb_descendants;
	}
	public int getColourNumber(){
		return colour_number;
	}
	public void setColourNumber(int colour_number){
		this.colour_number = colour_number;
	}


	/** Methods */
	public int getDescendantColourNumber(){
		if (getColourNumber() >= 159)
			return 160;
		else
			return getColourNumber() + 1;
	}

	public void reproductionFibo(){
		if (getMaturity() == 0)
			setMaturity(1);
		else{
			for (int i = 0; i < getNbDescendants(); i++){
				getDescendants().get(i).reproductionFibo();
			}
			getDescendants().add(0, (new Rabbit(0, getDescendantColourNumber())));
			setNbDescendants(getNbDescendants() + 1);
		}
	}

	public void displayRabbit(){
		int r = 255 - (20 * getColourNumber() * 255 / 159);
		int g = 20 * getColourNumber() * 510 / 159;
		int b = 20 * getColourNumber() * 255 / 159;
		if (g > 255)
			g = 510 - g;

		if (getMaturity() == 1)
			System.out.printf("\033[48;2;%d;%d;%dmO%d\033[0m ", r, g, b, getColourNumber());
		else
			System.out.printf("\033[48;2;%d;%d;%dmo%d\033[0m ", r, g, b, getColourNumber());
	}

	public void displayFamily(){
		displayRabbit();
		for (int i = 0; i < getNbDescendants(); i++){
			getDescendants().get(i).displayFamily();
		}
	}


	public static void main(String[] argv){}
}