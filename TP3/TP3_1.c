#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mt19937ar/mt19937ar.h"
#include "TP3_1.h"

		// TP 3 : Lab # 3 - Learning and Assessment Situations
		//        Monte Carlo Simulation & Confidence Intervals


/* uniform(): Draw a random number belonging to the [a - b] interval, with a uniform probability.
 *
 * Parameters: a, b -> two doubles such as a < b; boundary markers for the interval
 *
 * Output: double; the random number drawn.
 */
double uniform(double a, double b){
	return a + (b - a) * genrand_real1();
}


// Question 1 :

/* simPi(): Compute PI with the Monte Carlo method : draw a certain amount of points in the [-1, 1] square and calculate the number of points within the unit circle 
 *
 * Parameter: number_of_points -> int
 *
 * Output: double; the approximation of PI.
 */
double simPi(int number_of_points){
	long int inside_the_disk = 0;

	for (int i = 0; i < number_of_points; i++){
		double x = uniform(-1.0, 1.0); // je pourrais prendre entre 0 et 1 pcq ca change rien
		double y = uniform(-1.0, 1.0);
		if (x*x + y*y <= 1)
			inside_the_disk ++;
	}

	return (double)inside_the_disk / number_of_points * 4;
}


// Question 2 :

/* replicates_array(): Repeat number_of_replicates times the experiment of simPi with number_of_points as input. Initialise the array and fill it.
 *
 * Parameters: number_of_replicates, number_of_points -> two ints
 *
 * Output: array of double; size:number_of_replicates, in each bin the result of a call of simPi.
 */
double *replicates_array(int number_of_replicates, int number_of_points){
	double *mean_array = (double *)malloc(number_of_replicates * sizeof(double));

	for (int i = 0; i < number_of_replicates; i++){
		mean_array[i] = simPi(number_of_points);
	}

	return mean_array;
}

/* mean_from_array(): Calculate the arithmetic mean from an array.
 *
 * Parameters: array -> array of int
 * 				array_size -> int
 *
 * Output: double.
 */
double mean_from_array(double *array, int array_size){
	double mean = 0.0;

	for (int i = 0; i < array_size; i++)
		mean += array[i];

	return mean / array_size;
}


// Question 3

/* confidence_interval_of_95(): Computing of confidence intervals around the simulated mean. 95 percent of the sample in inside this interval.
 *
 * Parameter: number_of_replicates -> int
 *
 * Output: no output; display the confidence interval of 95 percent.
 */
void confidence_interval_of_95(int number_of_replicates){
	int n = 50;
	int number_of_points = 1000000;
	double *array_of_means = (double *)malloc(n * sizeof(double));
	for (int i = 0; i < n; i++){
		array_of_means[i] = mean_from_array(replicates_array(number_of_replicates, number_of_points), number_of_replicates);
	}
	
	// Compute of S²(n) (estimate without bias of the variance)
	double mean_of_means = mean_from_array(array_of_means, n);
	double s = 0.0;
	for (int i = 0; i < n; i++){
		s += (array_of_means[i] - mean_of_means) * (array_of_means[i] - mean_of_means);
	}
	s = s / (n - 1);

	// Compute of R (confidence radius (error margin))
	double r = 2.021 * sqrt(s / n); // the value 2.021 is given in the table

	printf("We have the following confidence interval of 95 percents (with %d means, of %d numbers of replicates and %d numbers of points each):\n", n, number_of_replicates, number_of_points);
	printf("mean = %f; R = %f; [mean - R; mean + R] = [%f; %f]\n", mean_of_means, r, mean_of_means - r, mean_of_means + r);
	free(array_of_means);
}

int main(void){
	// gcc -Wall -Wextra -Werror -g -O2 -o bonjour1.out mt19937ar/mt19937ar.c TP3_1.c -lm
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    
    printf("Tests de la question 1:\n");
    printf("Calculation of PI using Monte Carlo method and Mersenne Twister randomness :\n");
    printf("\tWith a sample of 1,000 points : %f\n", simPi(1000));
    printf("\tWith a sample of 1,000,000 points : %f\n", simPi(1000000));
    printf("\tWith a sample of 1,000,000,000 points : %f\n", simPi(1000000000));

    printf("\nTests de la question 2:\n");
    printf("Calculation of PI using the mean of the previous method :\n");
    double *array = replicates_array(10, 1000);
    printf("\tWith 10 replicates of a sample of 1,000 points : %f\n", mean_from_array(array, 10));
    array = replicates_array(10, 1000000);
    printf("\tWith 10 replicates of a sample of 1,000,000 points : %f\n", mean_from_array(array, 10));
    array = replicates_array(10, 1000000000);
    printf("\tWith 10 replicates of a sample of 1,000,000,000 points : %f\n", mean_from_array(array, 10));
    array = replicates_array(40, 1000000);
    printf("\tWith 40 replicates of a sample of 1,000,000 points : %f\n", mean_from_array(array, 40));
    printf("Do the result equals M_PI?: %d; myResult - M_PI = %f\n", mean_from_array(array, 40) == M_PI, mean_from_array(array, 40) - M_PI);
    printf("Relative error: myResult / M_PI = %f\n", mean_from_array(array, 40) / M_PI);
	free(array);
	
	printf("\nTests de la question 3:\n");
	confidence_interval_of_95(40);
	confidence_interval_of_95(80);
	confidence_interval_of_95(150);
	

	return 0;
}